<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }
	 
	 public function record_count() {
        return $this->db->count_all("users");
    }

     //get the username & password from tbl_usrs
     function get_user($limit, $start)
     {
          $sql = "select * from users limit $start,$limit";
          $query = $this->db->query($sql);			
          return $query->result();
     }

	 function add_user($data)
     {          
		  $this->db->insert('users', $data); 			         
     }
	 function add_path($data)
	 {
		$this->db->insert('map_paths', $data); 			         
	 }

	function get_map_paths($limit, $start, $uid)
	{
		$sql = "select * from map_paths where user_id='$uid' limit $start,$limit";
        $query = $this->db->query($sql);			
        return $query->result();
	 }
	 public function map_paths_count($id) {
        
		$this->db->where('user_id', $id);
		$this->db->from('map_paths');
		return $this->db->count_all_results();
    }

	function get_a_path($id)
	{
		$sql = "select path from map_paths where id='$id' ";
        $query = $this->db->query($sql);			
        return $query->result();
	}

	function get_all_paths($id)
	{
		$sql = "select * from map_paths where user_id='$id'";
        $query = $this->db->query($sql);			
        return $query->result();
	}
}?>