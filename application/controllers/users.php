<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class users extends CI_Controller
{

     public function __construct()
     {
          parent::__construct();
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
		  $this->load->library("pagination");
		  $this->load->model('user_model');
		  if(!$this->session->userdata('username'))
			 redirect('login');
		  
     }

     public function index()
     {
		/**************** pagination starts ****************/
		$config = array();
        $config["base_url"] = base_url() . "users/index/";
        $config["total_rows"] = $this->user_model->record_count();
        $config["per_page"] = 3;
        $config["uri_segment"] = 3;
		$this->pagination->initialize($config); 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		/**************** pagination ends ****************/

		$usr_result['users'] = $this->user_model->get_user($config["per_page"], $page);
		$data["results"] = $this->user_model->get_user($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

		 
		 $this->load->view('header');
		 $this->load->view('user/user_index', $data);
		 $this->load->view('footer');
     }

	 public function add()
     {
		 $this->load->view('header');
		 $this->load->view('user/user_add');
		 $this->load->view('footer');
		 if($_POST['btn_login'])
		 {
			  $data['user_name']=$_POST['txt_username'];
			  $data['user_id']=$_POST['txt_user_id'];
			  $data['password']=md5($_POST['txt_password']);
			  $this->user_model->add_user($data);
			  redirect('users/');
		 }
     }


	 

	public function showmap()
	{
		$this->load->library('googlemaps');
		
		$config['center'] = '60.1675, 24.9311';
		$config['zoom'] = '14';
		$config['draggable']=FALSE;
		$config['map_width'] = '100%';
		//$config['onclick'] = 'alert(\'You just clicked at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';
		$config['onclick'] = 'saveclicks(event.latLng.lat() + \', \' + event.latLng.lng());createMarker_map({ map: map, position:event.latLng });';
		$this->googlemaps->initialize($config);	
		$data['map'] = $this->googlemaps->create_map();		
		$this->load->view('header');
		$this->load->view('user/show_map', $data);
		$this->load->view('footer');
		
	}

	public function savepath()
	{
		$this->load->library('googlemaps');
		$data = $this->input->post('allval');
		$result = array_filter($data);			
		foreach($result as $key=>$val)
		{
			if(trim($val['value'])!='')
				$latlan[]=  $val['value'];
	
		}
		//print_r($latlan);
		$mapPaths = serialize($latlan);		
		$ajaxdata['user_id']=$this->session->userdata('userid');
		$ajaxdata['path']=$mapPaths;
		$ajaxdata['date']=date('Y:m:d H:i:s');
		
		//print_r($ajaxdata);
		$this->user_model->add_path($ajaxdata);
		//die;
		$this->load->library('googlemaps');
		
		$config['center'] = '60.1675, 24.9311';
		$config['zoom'] = '14';
		$config['draggable']=FALSE;
		$config['drawing']=true;
		$config['drawingModes']=array('polyline','marker');
		$config['map_width'] = '100%';
		//$config['onclick'] = 'alert(\'You just clicked at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';
		$config['onclick'] = 'saveclicks(event.latLng.lat() + \', \' + event.latLng.lng());createMarker_map({ map: map, position:event.latLng });';
		$this->googlemaps->initialize($config);	
		$polyline = array();
		$polyline['points'] = $latlan;
		$this->googlemaps->add_polyline($polyline);
		
		foreach($latlan as $k=>$v)
		{
			$marker = array();
			$marker['position'] = $v;
			$this->googlemaps->add_marker($marker);
		}
		//$this->googlemaps->add_marker($marker);


		$data['maps'] = $this->googlemaps->create_map();
		$data['latlan']=$result;
		
		
		$this->load->view('user/show_saved_map', $data);
		
		
	}


	public function savepath1()
	{
		
		$data = $this->input->post('hmlocation');	
		//$data = $this->input->post('allval');
		$result = array_filter($data);			
		$mapPaths = serialize($result);		
		$ajaxdata['user_id']=$this->session->userdata('userid');
		$ajaxdata['path']=$mapPaths;
		$ajaxdata['date']=date('Y:m:d H:i:s');
		
		//print_r($ajaxdata);
		$this->user_model->add_path($ajaxdata);
		$this->load->library('googlemaps');
		
		$config['center'] = '60.1675, 24.9311';
		$config['zoom'] = '14';
		$config['draggable']=FALSE;
		$config['drawing']=true;
		$config['drawingModes']=array('polyline','marker');
		$config['map_width'] = '100%';
		//$config['onclick'] = 'alert(\'You just clicked at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';
		$config['onclick'] = 'saveclicks(event.latLng.lat() + \', \' + event.latLng.lng());createMarker_map({ map: map, position:event.latLng });';
		$this->googlemaps->initialize($config);	
		$polyline = array();
		$polyline['points'] = $result;
		$this->googlemaps->add_polyline($polyline);
		
		foreach($result as $k=>$v)
		{
			$marker = array();
			$marker['position'] = $v;
			$this->googlemaps->add_marker($marker);
		}
		//$this->googlemaps->add_marker($marker);


		$data['map'] = $this->googlemaps->create_map();
		$data['latlan']=$result;
		
		$this->load->view('header');
		$this->load->view('user/save_map_path', $data);
		$this->load->view('footer');
		

	}

	public function ajax_post_controller()
	{
		$data = $this->input->post('allval');
		$result = array_filter($data);		
		foreach($result as $key=>$val)
		{
			if(trim($val['value'])!='')
				$latlan[]=  $val['value'];
	
		}		
		$opdat['data']=$latlan;
		$this->load->view('user/ajax_view', $opdat);
	}



	public function showpaths()
	{
		//echo 'sss'.$this->session->userdata('userid');
		/**************** pagination starts ****************/
		$config = array();
        $config["base_url"] = base_url() . "users/showpaths/";
        $config["total_rows"] = $this->user_model->map_paths_count($this->session->userdata('userid'));
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
		$this->pagination->initialize($config); 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		/**************** pagination ends ****************/

		$usr_result['users'] = $this->user_model->get_map_paths($config["per_page"], $page, $this->session->userdata('userid'));
		$data["results"] = $this->user_model->get_map_paths($config["per_page"], $page, $this->session->userdata('userid'));
		//echo count($data["results"]);
        $data["links"] = $this->pagination->create_links();
		$this->load->view('header');
		$this->load->view('user/showpaths', $data);
		$this->load->view('footer');
	}

	public function viewsavedpath()
	{
		
		$dataop=$this->user_model->get_a_path($this->uri->segment(3));
		$path=unserialize($dataop[0]->path);		
		$this->load->library('googlemaps');
		//echo json_encode($path);
		$config['center'] = '60.1675, 24.9311';
		$config['zoom'] = '14';
		$config['draggable']=FALSE;
		$config['drawing']=true;
		$config['drawingModes']=array('polyline','marker');
		$config['map_width'] = '100%';
		$config['onclick'] = 'saveclicks(event.latLng.lat() + \', \' + event.latLng.lng());createMarker_map({ map: map, position:event.latLng });';
		//$config['onclick'] = 'saveclicks(event.latLng.lat() + \', \' + event.latLng.lng());';
		$this->googlemaps->initialize($config);	
		$polyline = array();
		$polyline['points'] = $path;
		$this->googlemaps->add_polyline($polyline);
		
		foreach($path as $k=>$v)
		{
			$marker = array();
			$marker['position'] = $v;
			$this->googlemaps->add_marker($marker);
		}
		//$this->googlemaps->add_marker($marker);


		$data['map'] = $this->googlemaps->create_map();
		$data['latlan']=$path;
		
		$this->load->view('header');
		$this->load->view('user/viewsavedpath', $data);
		$this->load->view('footer');


	}


	public function exportJson()
	{
		$this->load->helper('download');
		//echo $this->session->userdata('userid');
		$usr_path = $this->user_model->get_all_paths($this->session->userdata('userid'));
		foreach($usr_path as $key=>$val)
		{
			$paths[]= unserialize($val->path);
		}
		//echo json_encode($paths);

		$downloadPath	=	BASEPATH.'downloads/';
		$fileName	=	$downloadPath.'paths.json';
		$jsonData	=	json_encode($paths);
		// WRITE THE CONTENT IN TO A FILE
		$fp = fopen($fileName, 'w');
		fwrite($fp, json_encode($jsonData));
		fclose($fp);

		$data = file_get_contents ( $fileName );
		 //force download
		 force_download($fileName, $data );

		// DOWNLOAD THE FILE
		@header('Content-disposition: attachment; filename=paths.json');
		@header('Content-type: application/json');
		echo $fileName;

		
	}
}
?>