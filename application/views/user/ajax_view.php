    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 450px;
		width: 50%;
      }
      /* Optional: Makes the sample page fill the window. */
      
      
    </style>
    <!--<div id="floating-panel">
      <button onclick="toggleHeatmap()">Toggle Heatmap</button>
      <button onclick="changeGradient()">Change gradient</button>
      <button onclick="changeRadius()">Change radius</button>
      <button onclick="changeOpacity()">Change opacity</button>
    </div>-->
	
    <div id="map" style="height: 450px; width: 100%"></div>
	<?php
	//print_r($data);
	$points='';
	foreach($data as $key=>$val)
	{
		echo "<input type=\"hidden\" name=\"hmlocation[]\" id=\"hmlocation$key\" value=\"new google.maps.LatLng($val)\">";
		$points = $points.' new google.maps.LatLng('.$val.'),';

	}
	
	?>
    <script>
	  // This example requires the Visualization library. Include the libraries=visualization
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=visualization">

      var map, heatmap;
	  
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: {lat: 60.1675,  lng: 24.9311},
          mapTypeId: 'roadmap'
        });

        heatmap = new google.maps.visualization.HeatmapLayer({
          data: [<?php echo $points;?>],
          map: map
        });
      }

      function toggleHeatmap() {
        heatmap.setMap(heatmap.getMap() ? null : map);
      }

      function changeGradient() {
        var gradient = [
          'rgba(0, 255, 255, 0)',
          'rgba(0, 255, 255, 1)',
          'rgba(0, 191, 255, 1)',
          'rgba(0, 127, 255, 1)',
          'rgba(0, 63, 255, 1)',
          'rgba(0, 0, 255, 1)',
          'rgba(0, 0, 223, 1)',
          'rgba(0, 0, 191, 1)',
          'rgba(0, 0, 159, 1)',
          'rgba(0, 0, 127, 1)',
          'rgba(63, 0, 91, 1)',
          'rgba(127, 0, 63, 1)',
          'rgba(191, 0, 31, 1)',
          'rgba(255, 0, 0, 1)'
        ]
        heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
      }

      function changeRadius() {
        heatmap.set('radius', heatmap.get('radius') ? null : 20);
      }

      function changeOpacity() {
        heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
      }


      // Heatmap data: 500 Points
      function getPoints() {
		  return [
          $('#hmlocation0').val(),
		  $('#hmlocation1').val(),
		  $('#hmlocation2').val(),
		  $('#hmlocation3').val(),
		  $('#hmlocation4').val(),
		  $('#hmlocation5').val(),
		  $('#hmlocation6').val(),
		  $('#hmlocation7').val(),
		  $('#hmlocation8').val(),
		  $('#hmlocation9').val(),
		  $('#hmlocation10').val()

          
        ];
      }

    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE879jPaG5ERGEreU9LPdoIrGko7J19Ss&libraries=visualization&callback=initMap">
    </script>
