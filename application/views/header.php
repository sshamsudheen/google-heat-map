<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Welcome:: Google Maps API</title>
     <!--link the bootstrap css file-->
     <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
     
     <style type="text/css">
     .colbox {
          margin-left: 0px;
          margin-right: 0px;
     }
     </style>
</head>
<body>
<div class="container">
     <div class="row">
          <div class="col-lg-6 col-sm-6">
               <h1>My Maps/Paths</h1>
          </div>
          <div class="col-lg-6 col-sm-6">
               
               <ul class="nav nav-pills pull-right" style="margin-top:20px">
                    
					<?php if($this->session->userdata('username')) {?>
					<li class="active"><a href="<?php echo base_url(); ?>login/logout">Logout</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>users/showpaths">Show my paths</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>users/showmap">Map</a></li>
					<?php 
					} 
					else
					{
					?><li class="active"><a href="#">Login</a></li>
					<li><a href="#">Signup</a></li>
					<?php 
					}?>
                    
               </ul>
               
          </div>
     </div>
</div>
<hr/>