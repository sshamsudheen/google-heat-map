-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 29, 2017 at 09:10 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `maps`
--

-- --------------------------------------------------------

--
-- Table structure for table `map_paths`
--

CREATE TABLE IF NOT EXISTS `map_paths` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `path` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `map_paths`
--


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `user_name` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `session` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `user_id`, `password`, `session`) VALUES
(1, 'admin', 'admin', 'admin', ''),
(2, 'test', 'test', 'test', ''),
(3, 'asdsadas', 'adminsadas', '963a13cfa6d4afe0953ebf66734761be', ''),
(4, 'dasdsa', 'sdsad', 'sfsd', ''),
(5, 'sdfds', 'dsdfsd', 'dsfsd', '');
